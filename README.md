# Travail écrit (G.F.)
Je n'ai pas écrit de code pour cet exercice portant sur la défragmentation de la base (j'ai vu la correction dont j'essaie de comprendre le code), mais je
présente les questions et idées qui se présentent à moi:
* Est-ce qu'une base de donnée sur un fichier .json tel que mise en place dans ce projet d'étude est envisageable dans un contexte professionnel avec d'intenses utilisations, 
plutôt qu'une  solution utilisant un SGBD (mariaDb, PostGresql...)?
* Est-ce que ce fichier .json (qui pourrait être terriblement volumineux) peut supporter des accès nombreux et concurrents de multiples utilisateurs ainsi que la maintenance simultanée ?
* faudrait-il prévoir les défragmentations lors d'heures creuses?
* Ou alors faudrait-il ajouter un attribut booléen "busy" à la classe qui s'occupe de défragmenter, qui empêcherait tout ajout ou retrait de badge et lecture de la base tant qu'il n'est pas passé à false? Autrement dit que les opérations en attente sont "bufferisées" pour être réalisées lorsque busy est à false?
Sinon est-ce que la solution apportée dans la correction (bloc6) permet une utilisation transparente et totalement sûre lors de défragmentation, d'ajout, retrait, et lecture dans la base, tout cele dans des laps de temps très courts?
* Lorsque l'on défragmente (dans la correction du bloc6) badgeId et walletPosition sont des metadatas qui changent, donc: faudrait-il rechercher un badge en fonction d'un Md5ash et 
d'un identifiant utilisateur et éventuellement une autre donnée, 
car il me semble que potentiellement des hash pour des gros fichiers différents pourraient très bien être identiques (car il y a beaucoup moins de Hash possibles que de fichiers volumineux possibles).

# Applications WEB, MVC, Services Rest
 ++ Pré-requis au Développement d'une application Web
 

## Objectifs
* Mises en application :
- [x] (Exercice 1) Service REST : Lecture d'un Badge
- [x] (Exercice 2) Service REST : suppression d'un Badge
- [x] (Exercice 3) Service REST : Ajout d'un Badge
- [X] **(Exercice 4) "Défragmentation" : Nettoyage des lignes supprimées par reconstitution d'une base propre**

----

Afin d'implémenter le service de suppression de badge, abordons le problème par impacts, couche par couche.

## Impacts

### Impact sur le Modèle

### Impact sur la désérialisation des Métadonnées

### Impact sur la sérialisation
      
### Impact sur Les DAOs
     
### Impact sur la couche de Service

## Tests

### Unitaires

### D'acceptance (de recette)

1. Ajout d'un badge avec valeurs par défaut
![](screenshots/put1.png)
2. Retour du service
![](screenshots/put2.png)
- On observe bien un code de retour 201, avec le lien vers l'instance créée
3. Consultation de l'instance créée
![](screenshots/put3.png)
- On observe bien l'objet demandé
4. Retour du service avec modification de la description
![](screenshots/put4.png)
- On observe bien le code retour 200
5. COnsultation de l'instance modifiée
![](screenshots/put5.png)
   - On observe bien la modification

  
**FIN**

----

## Ressources supplémentaires


## Défragmentation  (REMIS à PLUS TARD => non noté pour le moment, mais sujet d'examen blanc potentiel)

- [ ] Développer un traitement "batch" permettant de nettoyer le fichier de base de données Json suite à un certain nombre de suppressions
    - [ ] Il s'agit de lire le fichier de base json octet par octet en sautant les lignes supprimées (comme nous en connaissons les positions), et d'écrire cela dans la foulée vers un nouveau fichier, temporaire
    - [ ] En post-traitement il conviendra d'archiver la base originale, puis de renommer le fichier temporaire du nom du fichier original








