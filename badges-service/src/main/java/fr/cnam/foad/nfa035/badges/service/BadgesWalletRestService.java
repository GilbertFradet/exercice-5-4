package fr.cnam.foad.nfa035.badges.service;

import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.util.Set;

import static org.springframework.http.MediaType.*;

/**
 *
 * Service CRUD pour l'accès au prtefeuille de badges
 *
 * Commentez-moi
 */
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/_badges")
public interface BadgesWalletRestService {

    /**
     * Ajout ou mise-à-jour d'un Badge => C + U
     *
     * @param badge le badge à écrire, ou plutôt ses métadonnées
     * @param file le fichier image du badge à écrire
     * @return ResponseEntity la réponse REST toujours
     */
    @Operation(summary = "Ajoute un Badge au Wallet",
            description = "Ajoute un nouveau badge au portefeuille, mais s'il existe déjà, le met simplement à jour"
    )
    @ApiResponse(responseCode = "201", description = "Le badge a été Ajouté")
    @ApiResponse(responseCode = "200", description = "Le badge a été Modifié")
    @ApiResponse(responseCode = "400", description = "La requête est mal formulée")
    @Tag(name = "Badge")
    @PutMapping(consumes = { MULTIPART_FORM_DATA_VALUE })
    ResponseEntity putBadge(DigitalBadge badge, MultipartFile file, HttpServletRequest request) throws ServletException;

    /**
     * Lecture d'un Badge => R
     *
     * @param badge le badge à lire
     * @return ResponseEntity la réponse REST toujours
     */
    @Operation(summary = "Récupère l'image d'un Badge du Wallet",
            description = "Retourne l'image correspondant au badge sous la forme d'un flux, s'il est bien dans le portefeuille"
    )
    @ApiResponse(responseCode = "200", description = "Le badge a été lu normalement")
    @ApiResponse(responseCode = "404", description = "Le badge n'existe pas")
    @ApiResponse(responseCode = "400", description = "La requête est mal formulée")
    @Tag(name = "Badge")
    @PostMapping
    ResponseEntity<StreamingResponseBody> readBadge(DigitalBadge badge, boolean attachment);

    /**
     * Lecture du Wallet => R
     *
     * @return ResponseEntity la réponse REST toujours
     */
    @Operation(summary = "Récupère le métadonnées du Wallet",
            description = "Récupère le métadonnées du portefeuille, c'est à dire l'index des badges qui s'y trouve"
    )
    @Tag(name = "Wallet")
    @GetMapping("/metas")
    ResponseEntity<Set<DigitalBadge>> getMetadata();

    /**
     * Lecture d'un badge du Wallet => R (métas uniquement)
     *
     * @return ResponseEntity la réponse REST toujours
     */
    @Operation(summary = "Récupère le métadonnées un badge",
            description = "Récupère le métadonnées d'un seul badge du Wallet à partir de son hash md5"
    )
    @Tag(name = "Wallet")
    @ApiResponse(responseCode = "200", description = "Les métadonnées du badge a bien été restiuées")
    @ApiResponse(responseCode = "404", description = "Le badge n'existe pas")
    @GetMapping(value = "/{badgeHash}")
    ResponseEntity<DigitalBadge> getBadge(String badgeHash);

    /**
     * Suppression d'un Badge => D
     *
     * @param badge le badge à supprimer
     * @return ResponseEntity la réponse REST toujours
     */
    @Operation(summary = "Supprime un Badge du Wallet",
            description = "Supprime le badge, s'il est présent, du portefeuille"
    )
    @ApiResponse(responseCode = "200", description = "Le badge a bien été supprimé")
    @ApiResponse(responseCode = "404", description = "Le badge n'existe pas")
    @ApiResponse(responseCode = "400", description = "La requête est mal formulée")
    @Tag(name = "Badge")
    @DeleteMapping
    ResponseEntity deleteBadge(DigitalBadge badge);



}
