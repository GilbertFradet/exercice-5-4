package fr.cnam.foad.nfa035.badges.wallet.dao.impl;

import fr.cnam.foad.nfa035.badges.fileutils.streaming.media.impl.WalletFrame;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.ImageStreamingSerializer;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.WalletSerializer;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.ImageDeserializerBase64DatabaseImpl;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.MetadataDeserializerDatabaseImpl;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.WalletDeserializerDirectAccessImpl;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.WalletSerializerDirectAccessImpl;
import fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalWallet;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * DAO simple pour lecture/écriture d'un badge dans un wallet à badges digitaux multiples
 * et PAR ACCES DIRECT, donc prenant en compte les métadonnées de chaque badges.
 */
@Component("directAccess")
public class DirectAccessBadgeWalletDAOImpl implements DirectAccessBadgeWalletDAO {

    private static final Logger LOG = LogManager.getLogger(DirectAccessBadgeWalletDAOImpl.class);

    private final File walletDatabase;

    /**
     * Constructeur élémentaire
     *
     * @param dbPath
     * @throws IOException
     */
    public DirectAccessBadgeWalletDAOImpl(@Value("#{ systemProperties['db.path'] ?: 'badges-wallet/src/test/resources/db_wallet_indexed.csv'}") String dbPath) throws IOException{
        this.walletDatabase = new File(dbPath);
    }

    /**
     * Permet d'ajouter le badge au Wallet
     * @deprecated Sur les nouveaux formats de base CVS, Utiliser de préférence addBadge(DigitalBadge badge)
     * @hidden
     * @param image
     * @throws IOException
     */
    @Override
    @Deprecated
    public void addBadge(File image) throws IOException{
        LOG.error("Non supporté");
        throw new IOException("Méthode non supportée, utilisez plutôt addBadge(DigitalBadge badge)");
    }


    /**
     * {@inheritDoc}
     *
     * @param badge
     * @throws IOException
     */
    @Override
    public void removeBadge(DigitalBadge badge) throws IOException {
        try(WalletFrame media = new WalletFrame(new RandomAccessFile(walletDatabase, "rw"))){
            WalletSerializer serializer = new WalletSerializerDirectAccessImpl(getWalletMetadata());
            serializer.rollback(badge, media);
        }
    }

    /**
    /**
     * Permet d'ajouter le badge au nouveau format de Wallet
     *
     * @param badge
     * @throws IOException
     */
    @Override
    public void addBadge(DigitalBadge badge) throws IOException{
        try(WalletFrame media = new WalletFrame(new RandomAccessFile(walletDatabase, "rw"))){
            ImageStreamingSerializer serializer = new WalletSerializerDirectAccessImpl(getWalletMetadata());
            serializer.serialize(badge, media);
        }
    }

    /**
     * Permet de récupérer le badge du Wallet
     * @deprecated Sur les nouveaux formats de base CVS, Utiliser de préférence getBadgeFromMetadata
     *
     * @param imageStream
     * @throws IOException
     */
    @Override
    @Deprecated
    public void getBadge(OutputStream imageStream) throws IOException{
        try(WalletFrame media = new WalletFrame(new RandomAccessFile(walletDatabase, "r"))) {
            new ImageDeserializerBase64DatabaseImpl(imageStream).deserialize(media);
        }
    }


    /**
     * {@inheritDoc}
     * @return DigitalWallet
     */
    @Override
    public DigitalWallet getWalletMetadata() throws IOException {
        try(WalletFrame media = new WalletFrame(new RandomAccessFile(walletDatabase, "r"))) {
            Set<DigitalBadge> badges =  new MetadataDeserializerDatabaseImpl().deserialize(media);
            Set<Long> deadLinesPositions = new HashSet<>();
            DigitalWallet wallet = new DigitalWallet(badges,deadLinesPositions);
            for (Iterator<DigitalBadge> iterator = badges.iterator(); iterator.hasNext();) {
                DigitalBadge badge = iterator.next();
                if (badge.getMetadata().getBadgeId() == -1){
                    iterator.remove();
                    deadLinesPositions.add(badge.getMetadata().getWalletPosition());
                }
            }
            return wallet;

        }
    }

    /**
     * {@inheritDoc}
     *
     * @param imageStream
     * @param meta
     * @throws IOException
     */
    @Override
    public void getBadgeFromMetadata(OutputStream imageStream, DigitalBadge meta) throws IOException {
        DigitalWallet metas = this.getWalletMetadata();
        try(WalletFrame media = new WalletFrame(new RandomAccessFile(walletDatabase, "rw"))) {
            new WalletDeserializerDirectAccessImpl(imageStream, metas.getAllBadges()).deserialize(media, meta);
        }
    }


}
