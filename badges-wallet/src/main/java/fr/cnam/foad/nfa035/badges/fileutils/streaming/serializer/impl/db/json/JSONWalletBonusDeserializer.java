package fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.json;

import fr.cnam.foad.nfa035.badges.fileutils.streaming.media.WalletFrameMedia;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.DirectAccessDatabaseDeserializer;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;

import java.io.IOException;

/**
 * Interface définissant le comportement attendu d'un objet servant à
 * la désérialisation d'un badge du Wallet au format JSON.
 * En effet, cette interface apporteun certain nombre de méthodes bien pratiques
 *
 */
public interface JSONWalletBonusDeserializer extends DirectAccessDatabaseDeserializer {

    /**
     * Permet de désérialiser à partir des métadonnées strictement intrinsèques au badge (métadonnées techniques)
     *
     * @param media
     * @param metas les métadonnées du badge
     * @throws IOException
     */
    DigitalBadge deserialize(WalletFrameMedia media, DigitalBadgeMetadata metas) throws IOException ;

    /**
     * Permet de désérialiser à partir de la position du badge dans le fichier csv
     *
     * @param media
     * @param pos la position de lecture dans le fichier csv
     * @throws IOException
     */
    DigitalBadge deserialize(WalletFrameMedia media, long pos) throws IOException;
}
