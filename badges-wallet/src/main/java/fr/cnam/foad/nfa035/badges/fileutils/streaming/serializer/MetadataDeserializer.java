package fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer;

import fr.cnam.foad.nfa035.badges.fileutils.streaming.media.WalletFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;

import java.io.IOException;
import java.util.Set;

/**
 * Interface définissant le comportement attendu d'un objet servant à la désérialisation des metadonnées du portefeuille.
 * Ces objets doivent donc désérialiser à partir d'un média typé,
 * et il doit être possible d'en obtenir un flux de lecture pour l'obtention du contenu désérialisé.
 *
 */
public interface MetadataDeserializer {

    /**
     * Méthode principale de désérialisation, qui dans ce cas spécifique commence la lecture de chaque ligne
     * pour s'arrêter au denier point-virgule avant d'enchainer sur la suivante, récupérant ainsi l'ensemble des métadonnées
     * une fois le fichier du wallet entièrement parcouru.
     *
     * @return Set<DigitalBadge>
     * @param media
     * @throws IOException
     */
    Set<DigitalBadge> deserialize(WalletFrameMedia media) throws IOException;

    /**
     * Récupère une instance de Badge supprimé selon une ligne d'enregistrement donnée
     *
     * @param line
     * @return l'instance de badge vide/supprimé
     */
    DigitalBadge markAsDeleted(String line);

}
