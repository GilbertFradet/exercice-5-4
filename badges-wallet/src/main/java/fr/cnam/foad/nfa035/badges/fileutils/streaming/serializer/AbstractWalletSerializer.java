package fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer;

import fr.cnam.foad.nfa035.badges.fileutils.streaming.media.WalletFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalWallet;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Set;
import java.util.stream.Collectors;

/**
 *
 * Classe abstraite visant à structurer/guider le développement dee manière rigoureuse
 *
 */
public abstract class AbstractWalletSerializer extends AbstractStreamingImageSerializer<DigitalBadge, WalletFrameMedia> implements  WalletSerializer<DigitalBadge, WalletFrameMedia> {

    /**
     * Impose l'accès aux métadonnées du Wallet
     * @return DigitalWallet les métadonnées du Wallet
     */
    public abstract DigitalWallet getMetas();

    /**
     * Sérialise une image depuis un support quelconque vers un media quelconque
     *
     * @param source
     * @param media
     * @throws IOException
     */
    @Override
    public abstract void serialize(DigitalBadge source, WalletFrameMedia media) throws IOException ;


    /**
     * {@inheritDoc}
     *
     * @param source
     * @param media
     * @throws IOException
     */
    @Override
    public void rollback(DigitalBadge source, WalletFrameMedia media) throws IOException {
        RandomAccessFile random = media.getChannel();
        Set<DigitalBadge> allBadges = getMetas().getAllBadges();
        if (!allBadges.contains(source)) {
            throw new IOException("Badge non présent dans le Wallet");
        }
        try(OutputStream os = media.getEncodedImageOutput()) {
            long pos = allBadges.stream().filter(b -> b.equals(source)).collect(Collectors.toList()).get(0).getMetadata().getWalletPosition();
            random.seek(pos);
            PrintWriter writer = new PrintWriter(os, true, StandardCharsets.UTF_8);
            writer.printf("%1$d;%2$s;", -1, pos);
        }
    }

}
