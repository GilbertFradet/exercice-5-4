package fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer;

import fr.cnam.foad.nfa035.badges.fileutils.streaming.media.ImageFrameMedia;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Classe abstraite visant à structurer/guider le développement de manière plus rigoureuse
 * Elle s'applique donc à tout objet dont les méthodes auraient pour effet la sérialisation d'une image,
 * quel qu'en soit le format ou le média/canal de destination.
 *
 * @param <M> Le Media de sérialisation,
 *           à partir duquel nous voulons désérialiser notre image en base 64,
 *           c'est-à-dire la récupérer à son format original.
 */
public abstract class AbstractStreamingImageDeserializer<M extends ImageFrameMedia> implements ImageStreamingDeserializer<M> {

    private OutputStream sourceOutputStream;

    /**
     * Désérialise une image depuis un media quelconque vers un support quelconque
     *
     * @param media
     * @throws IOException
     */
    @Override
    public void deserialize(M media) throws IOException {
        //
        try(OutputStream os = getSourceOutputStream()){
            getDeserializingStream(media).transferTo(os);
        }
    }

    /**
     * {@inheritDoc}
     *
     * @param os
     * @param <T>
     */
    @Override
    public <T extends OutputStream> void setSourceOutputStream(T os) {
        this.sourceOutputStream = os;
    }

    /**
     * {@inheritDoc}
     *
     * @return le flux d'écriture
     */
    @Override
    public <T extends OutputStream> T getSourceOutputStream() {
        return (T) sourceOutputStream;
    }
}
