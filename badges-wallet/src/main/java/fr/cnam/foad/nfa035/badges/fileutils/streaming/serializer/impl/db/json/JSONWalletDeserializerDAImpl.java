package fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.json;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.media.WalletFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;
import org.apache.commons.codec.binary.Base64InputStream;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Set;

/**
 * Implémentation Base64 de désérialiseur de Badge Digital, basée sur des flux sur base JSON.
 */
public class JSONWalletDeserializerDAImpl implements JSONWalletBonusDeserializer {

    private static final Logger LOG = LogManager.getLogger(JSONWalletDeserializerDAImpl.class);

    private OutputStream sourceOutputStream;
    Set<DigitalBadge> metas;

    /**
     * Constructeur élémentaire
     * @param sourceOutputStream
     * @param metas les métadonnées du wallet, si besoin
     */
    public JSONWalletDeserializerDAImpl(OutputStream sourceOutputStream, Set<DigitalBadge> metas) {
        this.setSourceOutputStream(sourceOutputStream);
        this.metas = metas;
    }

    /**
     * {@inheritDoc}
     * Avec potentielle détection de fraude si les Metadonnées fournies ne correspondent pas dans leur intégralité...
     *
     * @param media
     * @param targetBadge le badge ( ou plutôt ses métadonnées )
     * @throws IOException
     */
    @Override
    public void deserialize(WalletFrameMedia media, DigitalBadge targetBadge) throws IOException {

        DigitalBadge badge = deserialize(media, targetBadge.getMetadata().getWalletPosition());
        if (!badge.equals(targetBadge)){
            throw new IOException ("Attention, il semble que le badge ciblé soit une tentative de fraude");
        }
        refreshBadgeMetadata(badge,targetBadge);
    }

    /**
     * {@inheritDoc}
     *
     * @param media
     * @param metas les métadonnées du badge
     * @throws IOException
     */
    @Override
    public DigitalBadge deserialize(WalletFrameMedia media, DigitalBadgeMetadata metas) throws IOException {

        DigitalBadge badge = deserialize(media, metas.getWalletPosition());
        return badge;
    }

    /**
     * Permet de rafraîchir les métadonnées d'un badge à partir de la source de données
     *
     * @param originBadge
     * @param targetBadge
     */
    private void refreshBadgeMetadata(DigitalBadge originBadge, DigitalBadge targetBadge) throws IOException {
        if(!(targetBadge.getSerial().equals(originBadge.getSerial()) && targetBadge.getEnd().equals(originBadge.getEnd()))){
            LOG.error("Badge Incohérent!");
            throw new IOException("Badge Incohérent!!");
        }
        targetBadge.setBegin(originBadge.getBegin());
        targetBadge.setMetadata(originBadge.getMetadata());
        targetBadge.setDescription(originBadge.getDescription());
    }

    /**
     * {@inheritDoc}
     *
     * @param media
     * @param pos la position de lecture dans le fichier csv
     * @throws IOException
     */
    @Override
    public DigitalBadge deserialize(WalletFrameMedia media, long pos) throws IOException {

        media.getChannel().seek(pos);

        JsonFactory jsonFactory = new JsonFactory();
        jsonFactory.configure(JsonGenerator.Feature.AUTO_CLOSE_TARGET,false);
        ObjectMapper objectMapper = new ObjectMapper(jsonFactory);

        String data = media.getEncodedImageReader(false).readLine();
        String badgeData = data.split(",\\{\"payload")[0].split(".*badge\":")[1];
        DigitalBadge badge = objectMapper.readValue(badgeData,DigitalBadge.class);

        String encodedImageData = data.split(",\\{\"payload\":")[1].split("\"\\}]")[0];
        // Désérialisation de l'image Base64 + écriture en clair dans le flux de restitution au format source
        try (OutputStream os = getSourceOutputStream()) {
            getDeserializingStream(encodedImageData).transferTo(os);
        }

        return badge;

    }

    /**
     * {@inheritDoc}
     * @return OutputStream
     */
    @Override
    public OutputStream getSourceOutputStream() {
        return sourceOutputStream;
    }

    /**
     * {@inheritDoc}
     * @param os
     */
    @Override
    public void setSourceOutputStream(OutputStream os) {
        this.sourceOutputStream = os;
    }


    /**
     * {@inheritDoc}
     *
     * Inutile => Non pris encharge par cet Objet
     *
     * @param media
     * @throws IOException
     */
    @Override
    public void deserialize(WalletFrameMedia media) throws IOException {
        // Inutile
        throw new IOException("Non pris encharge par cet Objet");
    }

    /**
     * {@inheritDoc}
     *
     * @param data
     * @return
     * @throws IOException
     */
    @Override
    public InputStream getDeserializingStream(String data) throws IOException {
        return new Base64InputStream(new ByteArrayInputStream(data.getBytes()));
    }
}
