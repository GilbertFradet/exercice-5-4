package fr.cnam.foad.nfa035.badges.gui.controller;

import fr.cnam.foad.nfa035.badges.gui.components.BadgePanel;
import fr.cnam.foad.nfa035.badges.gui.components.BadgesPanelFactory;
import fr.cnam.foad.nfa035.badges.gui.model.BadgesModel;
import fr.cnam.foad.nfa035.badges.gui.view.BadgeWalletGUI;
import fr.cnam.foad.nfa035.badges.gui.view.DisplayedBadgeHolder;
import fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * Commentez-moi
 */
@Component("badgesWalletController")
public class BadgeWalletController {

    @Autowired
    private BadgesPanelFactory badgesPanelFactory;

    @Qualifier("guiSelected")
    @Autowired
    private DirectAccessBadgeWalletDAO dao;

    @Autowired
    private DisplayedBadgeHolder displayedBadgeHolder;

    /**
     * Commentez-moi
     * @param view la vue
     * @throws IOException l'exception
     */
    public void delegateUIComponentsCreation(BadgeWalletGUI view) throws IOException {
        // 1. Le Model
        Set<DigitalBadge> metaSet = new TreeSet<>(dao.getWalletMetadata().getAllBadges());
        List<DigitalBadge> tableList = new ArrayList<>();
        tableList.addAll(metaSet);
        view.setTableList(tableList);
        // Badge initial
        displayedBadgeHolder.setDisplayedBadge(tableList.get(0));
    }

    /**
     * Commentez-moi
     */
    public void delegateUIManagedFieldsCreation(BadgeWalletGUI view) {
        BadgePanel badgePanel = badgesPanelFactory.createInstance();
        badgePanel.setPreferredSize(new Dimension(256, 256));
        view.setBadgePanel(badgePanel);
        JPanel panelImageContainer = new JPanel();
        view.setPanelImageContainer(panelImageContainer);
        panelImageContainer.add(badgePanel);
    }


    /**
     * Commentez-moi
     * @param badge le badge
     */
    public void setAddedBadge(DigitalBadge badge, BadgeWalletGUI view) {
        BadgesModel tableModel = view.getTableModel();
        tableModel.addBadge(badge);
        tableModel.fireTableDataChanged();
        loadBadge(tableModel.getRowCount()-1, view);
    }

    /**
     * Commentez-moi
     * @param row la ligne
     */
    public void loadBadge(int row, BadgeWalletGUI view){

        displayedBadgeHolder.setDisplayedBadge(view.getTableModel().getBadges().get(row));
        //obtention des objets de la vue
        JPanel panelHaut = view.getPanelHaut();
        JScrollPane scrollHaut = view.getScrollHaut();
        // attention effectuer cette initialisation avant la ligne suivante, sinon, piège....
        delegateUIManagedFieldsCreation(view);
        JPanel panelImageContainer = view.getPanelImageContainer();

        panelHaut.removeAll();
        panelHaut.revalidate();

        view.getTable1().setRowSelectionInterval(row, row);
        panelHaut.add(scrollHaut);
        panelImageContainer.setPreferredSize(new Dimension(256, 256));
        scrollHaut.setViewportView(panelImageContainer);

        panelHaut.repaint();
    }
}
